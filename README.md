# CarCar

Team:

* Person 1 - Chong Vang (Sales/Inventory)
* Person 2 - Brandon Downs (Service/Inventory)

## Getting Started
1. Open Terminal
2. Navigate to the Directory you intend on cloning the project into
3. Clone the Repository: ```git clone https://gitlab.com/Chong.Vang/project-beta```
4. Change Directories into the Cloned Repo.
5. Run the following commands in order
    * ```docker volume create beta-data```
    * ```docker-compose build```
    * ```docker-compose up```
6. Open Docker Desktop to Ensure all Containers are Running.    
7. Navigate to localhost:3000 once all Docker containers are running.
8. You should now see the CarCar Landing Page.

### Endpoints

| Action | Method | URL
|---------- | ----- | -------------- |
| List Manufacturers | GET | http://localhost:8100/api/manufacturers/
| Create a Manufacturer | POST | http://localhost:8100/api/manufacturers/
| Get Manufacturer Details | GET | http://localhost:8100/api/manufacturers/id/
| Update a Manufacturer | PUT | http://localhost:8100/api/manufacturers/id/
| Delete a Manufacturer | DELETE | http://localhost:8100/api/manufacturers/id/
| List Vehicle Models | GET | http://localhost:8100/api/models/
| Create a Vehicle Model | POST | http://localhost:8100/api/models/
| Get Model Details | GET | http://localhost:8100/api/models/id/
| Update a Model | PUT | http://localhost:8100/api/models/id/
| Delete a Model | DELETE | http://localhost:8100/api/models/id/
| List Automobiles | GET | http://localhost:8100/api/automobiles/
| Create an Automobile | POST | http://localhost:8100/api/automobiles/
| Get Automobiles Details | GET | http://localhost:8100/api/automobiles/vin/
| Update an Automobile | PUT | http://localhost:8100/api/automobiles/vin/
| Delete an Automobile | DELETE | http://localhost:8100/api/automobiles/vin/
| List Customers | GET | http://localhost:8090/api/customers/
| Create a Customer | POST | http://localhost:8090/api/customers/
| Delete a Customer | DELETE | http://localhost:8090/api/customers/id/
| List Salespeople | GET | http://localhost:8090/api/salespeople/
| Create a Salesperson | POST | http://localhost:8090/api/salespeople/
| Delete a Salesperson | DELETE | http://localhost:8090/api/salesperson/id/
| List all Sales | GET | http://localhost:8090/api/sales/
| Create New Sale | POST | http://localhost:8090/api/sales/
| Delete a Sale | DELETE | http://localhost:8090/api/sales/id/
| List Technicians | GET | http://localhost:8080/api/technicians/
| Technician Details | GET | http://localhost:8080/api/technicians/<int:pk>/
| Create Technician | POST | http://localhost:8080/api/technicians/
| Delete a Technicain | DELETE | http://localhost:8080/api/technicians/<int:pk>/
| List Service Appointments | GET | http://localhost:8080/api/serviceappointment/
| Service Appointment Detail | GET | http://localhost:8080/api/serviceappointment/<int:id>
| Service Appointment History | GET | http://localhost:8080/api/servicehistory/<int:vin>
| Create Service Appointment | POST | http://localhost:8080/api/serviceappointment/
| Delete Service Appointment | DELETE | http://localhost:8080/api/serviceappointment/<int:id>




## Design
![Design Context](images/Project%20Beta%20Excalidraw.png)
## Service Microservice

The Service Microservice empowers the User to create Vehicle appointments. It interacts with the inventory microservice via a Poller, which attains data about the vehicles within the Database. This includes information such as VIN, The "Sold" Status, and details about the Make/Model of the Vehicle. When an appointment is created, the system automatically records the appointment status as created, therefor tracking it in the Appointment List. The microservice tracks all service appointments and the relative data, such as customer, vehicle, reason for service, the assigned tech, and their VIP status. The Service MS is an essential part of the CarCar application, enabling an easy to utilize and understand method of Creating, Canceling, and Finishing Service Appointments.

The Service MS has three models that define data that is circulated throughout the Application.

* #### AutomobileVO:
    * Vin-A Unique, 17 Character Field that is accessed by the poller to identify each Automobile.
    * Sold-A boolean field default set to False. Which is used in conjunction with the sales ms to update the status of a vehicle in the inventory.


* #### Technician:
    * Employee Id- A unique identifier for the Employee, used to track what technician is assigned to a service.
    * First Name- Technicains First Name
    * Last Name- Technicians Last Name


* #### Appointment:
    * Date Time- Field for the Scheduled Appointment Time.
    * Reason- The Reason for the Appointment.
    * Status- The Status of the Appointment, whether created, canceled, or finished.
    * Vin-Unique 17 Char Field.
    * VIP-Defaults to False, checks if a VIN matches to one in the inventory, sold or unsold.
    * Customer-Field For Customer that the service is for.
    * Technician-Foreign Key linking the Appointment Model to the Technician model to retrieve relevant information. (Many to One)



## Sales Microservice

Explain your models and integration with the inventory
microservice, here.

<h1>Sales Microservice</h1>
The sales microservice keeps track of the automobile, salesperson, & customer data. There are (4) models within the sales models; AutomobileVO, Salesperoson, Customer, Sale
The poller retrieves the AutomobileVO model data from the Inventory Automobile.

<h2>Salesperson Model</h2>
<h3>Endpoints</h3>
<p>List salespeople: GET http://localhost:8090/api/salespeople/</p>
<p>Create salesperson: POST http://localhost:8090/api/salespeople/</p>
<p>Delete a salesperson: DELETE http://localhost:8090/api/salespeople/<salesperson.id>/</p>

<div>Create Salesperson:</div>
<img src="images/Createsalesperson.jpg">

<div>List of Salesperson(s):</div>
<img src="images/ListofSalespersons.jpg">

<div>Deleting of Salesperson:</div>
<img src="images/ListofSalespersons.jpg">

<h2>Customer Model</h2>
<h3>Endpoints</h3>
<p>List customers: GET http://localhost:8090/api/customers/</p>
<p>Create new customer: POST http://localhost:8090/api/customers/</p>
<p>Delete a customer: http://localhost:8090/api/customers/<customer.id>/</p>

<div>Create Customer:</div>
<img src="images/CreateCustomer1.jpg">

<div>List of Customer(s):</div>
<img src="images/CustomerList.jpg">

<div>Deleting of Customer:</div>
<img src="images/DeleteCustomer.jpg">

<h2>Sale Model</h2>
<h3>Endpoints</h3>
<p>List sales: GET http://localhost:8090/api/sales/</p>
<p>Create new sale: POST http://localhost:8090/api/sales/</p>
<p>Delete a sale: DELETE http://localhost:8090/api/sales/<employee.id>/</p>

<div>Create Sale:</div>
<img src="images/CreateSale.jpg">

<div>List of Sale(s):</div>
<img src="images/SalesList.jpg">

<div>Deleting of Sale:</div>
<img src="images/DeleteSale.jpg">


<h1>AutomobileVO:<h1>
<p>The AutomobileVO model retrieves data object from the Inventory using a poller. AutomobileVO contains two keys(vin, sold) with values set to the user input.</p>
