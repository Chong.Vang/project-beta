from django.shortcuts import render
from.models import AutomobileVO, Salesperson, Customer, Sale
from django.views.decorators.http import require_http_methods
import json
from django.http import JsonResponse
from common.json import ModelEncoder


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
    ]


class SalesPersonEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "id",
    ]


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "first_name",
        "last_name",
        "address",
        "phone_number",
        "id",
    ]


class SaleListEncoder(ModelEncoder):
    model = Sale
    properties = [
        "price",
        "automobile",
        "salesperson",
        "customer",
        "id",
    ]
    encoders = {
        "automobile": AutomobileVOEncoder(),
        "salesperson": SalesPersonEncoder(),
        "customer": CustomerEncoder(),
    }


@require_http_methods(["GET"])
def api_auto_list(request):
    if request.method == "GET":
        autos = AutomobileVO.objects.all()
        return JsonResponse(
            {"autos": autos},
            encoder=AutomobileVOEncoder,
            sold=False,
        )


@require_http_methods(["GET", "POST"])
def api_sales_list(request):
    sales = []
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SaleListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        try:
            salesperson = content["salesperson"]
            sale = Salesperson.objects.get(id=salesperson)
            content["salesperson"] = sale
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Sales personnel does not exist"},
                status=400,
            )
        try:
            customer = content["customer"]
            purchase = Customer.objects.get(id=customer)
            content["customer"] = purchase
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer does not exist"},
                status=400,
            )
        try:
            vin = content["automobile"]
            auto = AutomobileVO.objects.get(vin=vin)
            content["automobile"] = auto
            content["automobile"].sold = True
            content["automobile"].save()
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Vehicle has been sold or does not exist"},
                status=400,
            )
        total_sale = Sale.objects.create(**content)
        return JsonResponse(
            total_sale,
            encoder=SaleListEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE"])
def api_delete_sales(request, pk):
    if request.method == "GET":
        sale = Sale.objects.get(id=pk)
        return JsonResponse(
            sale,
            encoder=SaleListEncoder,
            safe=False,
        )
    else:
        try:
            delete_sale = Sale.objects.get(id=pk)
            delete_sale.delete()
            return JsonResponse(
                delete_sale,
                encoder=SaleListEncoder,
                safe=False,
            )
        except Sale.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})


@require_http_methods(["GET", "POST"])
def api_salesperson_list(request):
    if request.method == "GET":
        sales_persons = Salesperson.objects.all()
        return JsonResponse(
            {"salesperson": sales_persons},
            encoder=SalesPersonEncoder,
            safe=False,
        )
    else:
        try:
            content = json.loads(request.body)
            sales_person = Salesperson.objects.create(**content)
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except (ValueError, TypeError):
            return JsonResponse(
                {"message": "Incorrect Data"},
                status=400,
            )


@require_http_methods(["GET", "DELETE"])
def api_delete_salesperson(request, pk):
    if request.method == "GET":
        salesperson = Salesperson.objects.get(id=pk)
        return JsonResponse(
            salesperson,
            encoder=SalesPersonEncoder,
            safe=False,
        )
    else:
        try:
            delete_salesperson = Salesperson.objects.get(id=pk)
            delete_salesperson.delete()
            return JsonResponse(
                delete_salesperson,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})


@require_http_methods(["GET", "POST"])
def api_customer_list(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customer": customers},
            encoder=CustomerEncoder,
            safe=False,
        )
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except (ValueError, TypeError):
            return JsonResponse(
                {"message": "Incorrect Data"},
                status=400,
            )


@require_http_methods(["GET", "DELETE"])
def api_delete_customer(request, pk):
    if request.method == "GET":
        customer = Customer.objects.get(id=pk)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )
    else:
        try:
            delete_customer = Customer.objects.get(id=pk)
            delete_customer.delete()
            return JsonResponse(
                delete_customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
