from django.urls import path
from .views import (
    api_sales_list,
    api_salesperson_list,
    api_customer_list,
    api_delete_sales,
    api_delete_salesperson,
    api_delete_customer,
    api_auto_list,
)

urlpatterns = [
    path("sales/", api_sales_list, name="sales_list"),
    path("salespeople/", api_salesperson_list, name="salesperson_list"),
    path("customers/", api_customer_list, name="customer_list"),
    path("sales/<int:pk>/", api_delete_sales, name="delete_sales"),
    path("salespeople/<int:pk>/", api_delete_salesperson, name="delete_salespeople"),
    path("customers/<int:pk>/", api_delete_customer, name="delete_customer"),
    path("automobiles/", api_auto_list, name="auto_list"),
]
