import React, {useState, useEffect} from 'react'

function SaleHistory({}) {
    const [saleHistory, setSaleHistory] = useState([]);
    const [search, setSearch] = useState("");

    async function getHistory() {
        const response = await fetch('http://localhost:8090/api/sales/');
        if (response.ok) {
          const data = await response.json();
          setSaleHistory(data.sales);
        } else {
          console.error('An error occurred')
        }
    }

    async function handleDelete(event, sale) {
        event.preventDefault();
        const url = `http://localhost:8090/api/sales/${sale}`;
        const fetchConfig = {
            method: "delete",
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            getHistory();
        }
    }

    useEffect(() => {
        getHistory();
    }, [])

    return (
        <div className='my-5 container'>
                <h5>Search by Employee ID</h5>
                <div className='text-left my-2'>
                <input
                value={search}
                type="text"
                onChange={e => setSearch(e.target.value)}
                placeholder='Employee ID'
                />
                <i className="bi bi-search"></i>
            </div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Price</th>
                        <th>Vin</th>
                        {/* <th>Sold</th> */}
                        <th>Sales Personnel</th>
                        <th>Employee ID</th>
                        {/* <th>Database ID</th> */}
                        <th>Customer</th>
                        <th>Address</th>
                        <th>Phone Number</th>
                        {/* <th>Customer Database ID</th> */}
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {saleHistory.filter(
                        sale => search === "" || sale.salesperson.employee_id.includes(search))?.map(
                            sale => {
                            return (
                                <tr key={sale.id}>
                                    <td>{ sale.price }</td>
                                    <td>{ sale.automobile.vin }</td>
                                    {/* <td>{ sale.automobile.sold ? 'True' : 'False'}</td> */}
                                    <td>{ sale.salesperson.first_name } { sale.salesperson.last_name }</td>
                                    <td>{ sale.salesperson.employee_id }</td>
                                    {/* <td>{ sale.salesperson.id }</td> */}
                                    <td>{ sale.customer.first_name } { sale.customer.last_name }</td>
                                    <td>{ sale.customer.address }</td>
                                    <td>{ sale.customer.phone_number }</td>
                                    {/* <td>{ sale.customer.id }</td> */}
                                    <td><button onClick={(event) => handleDelete(event, sale.id)}>Delete</button></td>
                                </tr>
                            );
                        })}
                </tbody>
            </table>
        </div>
    );
}

export default SaleHistory;
