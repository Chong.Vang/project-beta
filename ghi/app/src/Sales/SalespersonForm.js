import React, { useState } from 'react';

function SalespersonForm({}) {
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [employeeId, setEmployeeId] = useState("");

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.first_name = firstName;
        data.last_name = lastName;
        data.employee_id = employeeId;

        const salesPersonUrl = 'http://localhost:8090/api/salespeople/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };
        const response = await fetch(salesPersonUrl, fetchConfig);
        if (response.ok) {
            setFirstName("");
            setLastName("");
            setEmployeeId("");
        }
    }
    function handleChangeFirstName(event) {
        const {value} = event.target;
        setFirstName(value);
    }
    function handleChangeLastName(event) {
        const {value} = event.target;
        setLastName(value);
    }
    function handleChangeEmployeeId(event) {
        const {value} = event.target;
        setEmployeeId(value);
    }
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a New Salesperson</h1>
                    <form onSubmit={handleSubmit} id="create-salesperson-form">
                    <div className="form-floating mb-3">
                            <input value={firstName} onChange={handleChangeFirstName} placeholder="First Name" required type="text" name="first_name" id="first_name" className="form-control" />
                            <label htmlFor="first_name">First Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={lastName} onChange={handleChangeLastName} placeholder="Last Name" required type="text" name="last_name" id="last_name" className="form-control" />
                            <label htmlFor="last_name">Last Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={employeeId} onChange={handleChangeEmployeeId} placeholder="Employee ID" required type="number" pattern="[0-9]" name="employee_id" id="employee_id" className="form-control" />
                            <label htmlFor="employee_id">Employee ID</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default SalespersonForm;
