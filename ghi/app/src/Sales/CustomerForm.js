import React, { useState } from 'react';

function CustomerForm({}) {
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [address, setAddress] = useState("");
    const [phoneNumber, setPhoneNumber] = useState("");

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.first_name = firstName;
        data.last_name = lastName;
        data.address = address;
        data.phone_number = phoneNumber;

        const salesPersonUrl = 'http://localhost:8090/api/customers/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };
        const response = await fetch(salesPersonUrl, fetchConfig);
        if (response.ok) {
            setFirstName("");
            setLastName("");
            setAddress("");
            setPhoneNumber("");
        }
    }
    function handleChangeFirstName(event) {
        const {value} = event.target;
        setFirstName(value);
    }
    function handleChangeLastName(event) {
        const {value} = event.target;
        setLastName(value);
    }
    function handleChangeAddress(event) {
        const {value} = event.target;
        setAddress(value);
    }
    function handleChangePhoneNumber(event) {
        const {value} = event.target;
        setPhoneNumber(value);
    }
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new Customer</h1>
                    <form onSubmit={handleSubmit} id="create-customer-form">
                    <div className="form-floating mb-3">
                            <input value={firstName} onChange={handleChangeFirstName} placeholder="First Name" required type="text" name="first_name" id="first_name" className="form-control" />
                            <label htmlFor="first_name">First Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={lastName} onChange={handleChangeLastName} placeholder="Last Name" required type="text" name="last_name" id="last_name" className="form-control" />
                            <label htmlFor="last_name">Last Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={address} onChange={handleChangeAddress} placeholder="Address" required type="text" name="address" id="address" className="form-control" />
                            <label htmlFor="address">Address</label>
                        </div>
                        <div className="form-floating mb-3">
                        <input value={phoneNumber} onChange={handleChangePhoneNumber} placeholder="Phone Number" required type="number" name="text" id="text" className="form-control" />
                            <label htmlFor="phone_number">Phone Number</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default CustomerForm;
