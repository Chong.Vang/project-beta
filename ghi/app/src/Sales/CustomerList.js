import {useState, useEffect} from 'react'

function CustomerList({props}) {
    const [customer, setCustomer] = useState([]);

    async function getCustomer() {
        const response = await fetch('http://localhost:8090/api/customers/');
        if (response.ok) {
          const data = await response.json();
          setCustomer(data.customer);
        } else {
          console.error('An error occurred')
        }
      }

    async function handleDelete(event, customer) {
        event.preventDefault();
        const url = `http://localhost:8090/api/customers/${customer}`;
        const fetchConfig = {
            method: "delete",
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            getCustomer();
        }
    }

    useEffect(() => {
        getCustomer();
    }, [])

    return (
        <>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Address</th>
                    <th>Phone Number</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                {customer?.map(customer => {
                    return (
                        <tr key={customer.id}>
                            <td>{ customer.first_name }</td>
                            <td>{ customer.last_name }</td>
                            <td>{ customer.address }</td>
                            <td>{ customer.phone_number }</td>
                            <td><button onClick={(event) => handleDelete(event, customer.id)}>Delete</button></td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
        </>
    );
}

export default CustomerList;
