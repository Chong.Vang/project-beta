import React, { useEffect, useState } from 'react';

function CreateSaleForm({}) {
    const [price, setPrice] = useState("");
    const [automobile, setAutomobile] = useState("");
    const [salesPerson, setSalesPerson] = useState("");
    const [customer, setCustomer] = useState("");
    const [autos, setAutos] = useState([]);
    const [customers, setCustomers] = useState([]);
    const [salesPeople, setSalesPeople] = useState([]);

    async function fetchAutos() {
        const autoUrl = 'http://localhost:8100/api/automobiles/';
        const response = await fetch(autoUrl);
        if (response.ok) {
            const data = await response.json();
            setAutos(data.autos);
        }
    }

    async function fetchCustomers() {
        const customerUrl = 'http://localhost:8090/api/customers/';
        const response = await fetch(customerUrl);
        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customer);
        }
    }

    async function fetchSalesPeople() {
        const autoSalesPeople = 'http://localhost:8090/api/salespeople/';
        const response = await fetch(autoSalesPeople);
        if (response.ok) {
            const data = await response.json();
            setSalesPeople(data.salesperson);
        }
    }

    useEffect(() => {
        fetchAutos();
        fetchCustomers();
        fetchSalesPeople();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.price = price;
        data.automobile = automobile;
        data.salesperson = salesPerson;
        data.customer = customer;

        const saleUrl = 'http://localhost:8090/api/sales/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        };
        const response = await fetch(saleUrl, fetchConfig);
        if (response.ok) {
            window.location.href = 'http://localhost:3000/sales/new'
            setPrice("");
            setAutomobile("");
            setSalesPerson("");
            setCustomer("");
        }
    }
    function handleChangePrice(event) {
        const {value} = event.target;
        setPrice(value);
    }
    function handleChangeAutomobile(event) {
        const {value} = event.target;
        setAutomobile(value);
    }
    function handleChangeSalesPerson(event) {
        const {value} = event.target;
        setSalesPerson(value);
    }
    function handleChangeCustomer(event) {
        const {value} = event.target;
        setCustomer(value);
    }
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create Sale</h1>
                    <form onSubmit={handleSubmit} id="create-sales-form">
                    <div className="form-floating mb-3">
                            <input value={price} onChange={handleChangePrice} placeholder="Price" required type="number" name="Price" id="Price" className="form-control" />
                            <label htmlFor="price">Price</label>
                        </div>
                        <div className="form-floating mb-3">
                            <select onChange={handleChangeAutomobile} placeholder="Automobile" name="Automobile" id="Automobile" className="form-control">
                            <option value="">Choose Automobile Vin</option>
                            {autos.map(autos => {
                                return (
                                    <option key={autos.vin} value={autos.vin}>{autos.vin}</option>
                                )
                            })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <select onChange={handleChangeSalesPerson} placeholder="Sales Personnel" name="Sales Personnel" id="Sales Personnel" className="form-control">
                            <option value= "">Choose Sales Personnel</option>
                            {salesPeople.map(salesperson => {
                                return (
                                    <option key={salesperson.id} value={salesperson.id}>{salesperson.first_name} {salesperson.last_name}</option>
                                )
                            })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <select onChange={handleChangeCustomer} placeholder="Customer" name="Customer" id="Customer" className="form-control">
                            <option value="">Choose Customer</option>
                            {customers.map(customer => {
                                return (
                                    <option key={customer.id} value={customer.id}>{customer.first_name} {customer.last_name}</option>
                                )
                            })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default CreateSaleForm;
