import {useState, useEffect} from 'react'

function SalesPersonList({props}) {
    const [salesPerson, setSalesPerson] = useState([]);

    async function getSalesPerson() {
        const response = await fetch('http://localhost:8090/api/salespeople/');
        if (response.ok) {
          const data = await response.json();
          setSalesPerson(data.salesperson);
        } else {
          console.error('An error occurred')
        }
      }

    async function handleDelete(event, salesPerson) {
        event.preventDefault();
        const url = `http://localhost:8090/api/salespeople/${salesPerson}`;
        const fetchConfig = {
            method: "delete",
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            getSalesPerson();
        }
    }

    useEffect(() => {
        getSalesPerson();
      }, [])

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Employee Id</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                {salesPerson?.map(salesPerson => {
                    return (
                        <tr key={salesPerson.id}>
                            <td>{ salesPerson.first_name }</td>
                            <td>{ salesPerson.last_name }</td>
                            <td>{ salesPerson.employee_id }</td>
                            <td><button onClick={(event) => handleDelete(event, salesPerson.id)}>Delete</button></td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
}

export default SalesPersonList;
