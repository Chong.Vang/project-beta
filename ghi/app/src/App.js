import { BrowserRouter, Routes, Route } from 'react-router-dom';
import React, { useEffect, useState } from "react";
import MainPage from './MainPage';
import Nav from './Nav';
import CustomerForm from './Sales/CustomerForm';
import CustomerList from './Sales/CustomerList';
import SalespersonForm from './Sales/SalespersonForm';
import SalespersonList from './Sales/SalespersonList';
import CreateSaleForm from './Sales/CreateSaleForm';
import SaleHistory from './Sales/SaleHistoryList';
import TechnicianForm from './Service/TechnicianForm';
import TechniciansList from './Service/TechnicianList';
import AppointmentForm from './Service/AppointmentForm';
import AppointmentsList from './Service/AppointmentList';
import ServiceHistory from './Service/ServiceHistory';
import ManufacturersList from './Inventory/ManufacturersList';
import AutomobilesList from './Inventory/AutomobileList';
import ModelsList from './Inventory/ModelList';
import AutomobileForm from './Inventory/CreateAutomobileForm';
import ManufacturerForm from './Inventory/CreateManufacturerForm';
import VehicleModelForm from './Inventory/CreateVehicleModelForm';


function App() {
  return (
    <BrowserRouter>
      <Nav />
        <Routes>
          <Route index element={<MainPage />}/>
          <Route path="customers/new" element={<CustomerForm />} />
          <Route path="customers/" element={<CustomerList />} />
          <Route path="salespeople/new" element={<SalespersonForm />} />
          <Route path="salespeople/" element={<SalespersonList />} />
          <Route path="sales/new" element={<CreateSaleForm />} />
          <Route path="sales/" element={<SaleHistory />} />
          <Route path="/" element={<MainPage />} />
          <Route path="technicians/create/" element={<TechnicianForm />} />
          <Route path="technicians/" element={<TechniciansList />} />
          <Route path="appointments/create/" element={<AppointmentForm />} />
          <Route path="appointments/" element={<AppointmentsList />}/>
          <Route path="appointments/history/" element={<ServiceHistory />}/>
          <Route path="models/create/" element={<VehicleModelForm />} />
          <Route path="manufacturers/create/" element={<ManufacturerForm />} />
          <Route path="automobiles/create/" element={<AutomobileForm />} />
          <Route path="manufacturers/list/" element={<ManufacturersList />}/>
          <Route path="automobiles/list/" element={<AutomobilesList />}/>
          <Route path="models/list/" element={<ModelsList />}/>
        </Routes>
    </BrowserRouter>
  );
}

export default App;
