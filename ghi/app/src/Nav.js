import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link" aria-current="page" to="/">Home</NavLink>
            </li>
            <li className="nav-item dropdown">
              <NavLink className="nav-link dropdown-toggle" to="#" id="automobilesDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Automobiles
              </NavLink>
              <ul className="dropdown-menu" aria-labelledby="automobilesDropdown">
                <li><NavLink className="dropdown-item" to="automobiles/list">Automobiles</NavLink></li>
                <li><NavLink className="dropdown-item" to="automobiles/create/">Create an Automobile</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <NavLink className="nav-link dropdown-toggle" to="#" id="modelsDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Models
              </NavLink>
              <ul className="dropdown-menu" aria-labelledby="modelsDropdown">
                <li><NavLink className="dropdown-item" to="models/list">Models</NavLink></li>
                <li><NavLink className="dropdown-item" to="models/create/">Create a Model</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <NavLink className="nav-link dropdown-toggle" to="#" id="manufacturersDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Manufacturers
              </NavLink>
              <ul className="dropdown-menu" aria-labelledby="manufacturersDropdown">
                <li><NavLink className="dropdown-item" to="manufacturers/list">Manufacturers</NavLink></li>
                <li><NavLink className="dropdown-item" to="manufacturers/create/">Add a Manufacturer</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <NavLink className="nav-link dropdown-toggle" to="#" id="customersDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Customers
              </NavLink>
              <ul className="dropdown-menu" aria-labelledby="customersDropdown">
                <li><NavLink className="dropdown-item" to="/customers/new">Add a Customer</NavLink></li>
                <li><NavLink className="dropdown-item" to="/customers/">Customers</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <NavLink className="nav-link dropdown-toggle" to="#" id="salespeopleDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Salespeople
              </NavLink>
              <ul className="dropdown-menu" aria-labelledby="salespeopleDropdown">
                <li><NavLink className="dropdown-item" to="/salespeople/new">Add a Salesperson</NavLink></li>
                <li><NavLink className="dropdown-item" to="/salespeople">Sales Personnel</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <NavLink className="nav-link dropdown-toggle" to="#" id="salesDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Sales
              </NavLink>
              <ul className="dropdown-menu" aria-labelledby="salesDropdown">
                <li><NavLink className="dropdown-item" to="/sales">Sales</NavLink></li>
                <li><NavLink className="dropdown-item" to="/sales/new">Sale Form</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <NavLink className="nav-link dropdown-toggle" to="#" id="techniciansDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Technicians
              </NavLink>
              <ul className="dropdown-menu" aria-labelledby="techniciansDropdown">
                <li><NavLink className="dropdown-item" to="technicians/create/">Add a Technician</NavLink></li>
                <li><NavLink className="dropdown-item" to="technicians/">Technicians</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <NavLink className="nav-link dropdown-toggle" to="#" id="appointmentsDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Appointments
              </NavLink>
              <ul className="dropdown-menu" aria-labelledby="appointmentsDropdown">
                <li><NavLink className="dropdown-item" to="appointments/create/">Create a Service Appointment</NavLink></li>
                <li><NavLink className="dropdown-item" to="appointments/">Service Appointments</NavLink></li>
                <li><NavLink className="dropdown-item" to="appointments/history/">Service History</NavLink></li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
